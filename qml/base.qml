import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.2
import "elements"
import "components"


ApplicationWindow {
    id: application
    visible: true
    title: "Title"
    width: 1920
    height: 1080
    background: Rectangle {
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#303030" }
            GradientStop { position: 1.0; color: "#191919" }
        }
    }

    Rectangle {
        id: workspace
        anchors.centerIn: parent
        width: 500
        height: 400
        color: "#333333"
        radius: 20

        Rectangle {
            id: textContainer
            anchors.top: workspace.top
            anchors.topMargin: 15
            height: 20
            width: workspace.width
            color: "transparent"

            CText {
                id: text
                anchors.centerIn: textContainer
                text: "Виконайте логін у програму"
            }
        }

        Line {
            id: line
            anchors.top: textContainer.bottom
            anchors.topMargin: 15
            width: workspace.width
        }

        ColumnLayout {
            id: column

            anchors {
                left: workspace.left
                right: workspace.right
                top: line.bottom
                bottom: workspace.bottom

                rightMargin: 40
                leftMargin: 40
            }

            ChooseDBRow {
                id: database
                Layout.alignment: Qt.AlignHCenter
            }

            CTextField {
                id: loginField
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter
                placeholderText: "Логін"
            }

            CTextField {
                id: passwordField
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignHCenter
                placeholderText: "Пароль"
                echoMode: TextInput.Password
            }

            Row {
                Layout.bottomMargin: 20
                Layout.alignment: Qt.AlignHCenter
                spacing: column.width - combobox.width - login.width

                CComboBox {
                    id: combobox
                    model: db.comboboxLocations

                    Component.onCompleted: {
                        db.getLocations()
                    }
                }

                LoginButton {
                    id: login
                    onClicked: {
                        console.log(
                            database.selectedDB,
                            loginField.text,
                            passwordField.text,
                            combobox.textAt(combobox.currentIndex)
                        )
                    }
                }
            }
        }
    }
}
