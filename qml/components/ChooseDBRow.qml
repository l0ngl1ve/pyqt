import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.2
import "../elements"


RowLayout {
    spacing: 20
    property string selectedDB: "debug_production"

    CText {
        text: "Database:"
    }

    CRadioButton {
        text: "production"
        onClicked: { selectedDB = "production" }
    }

    CRadioButton {
        checked: true
        text: "debug_production"
        onClicked: { selectedDB = "debug_production" }
    }
}
