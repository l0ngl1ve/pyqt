import QtQuick 2.7
import QtQuick.Controls 2.2

ComboBox {
    id: combobox

    contentItem: Text {
        leftPadding: 0
        rightPadding: combobox.indicator.width + combobox.spacing

        text: combobox.displayText
        color: "#5ae4aa"
        font: combobox.font
        elide: Text.ElideRight
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    background: Rectangle {
        implicitWidth: 120
        implicitHeight: 50
        radius: 10
        color: "#4a4a4a"
    }

    delegate: ItemDelegate {
        width: combobox.width
        highlighted: combobox.highlightedIndex === index

        contentItem: Text {
            text: modelData
            color: "white"
            font: combobox.font
            elide: Text.ElideRight
            verticalAlignment: Text.AlignVCenter
        }

        background: Rectangle {
            implicitWidth: 120
            implicitHeight: 50
            radius: 10
            color: highlighted ? "gray" : "transparent"
        }
    }

    indicator: Canvas {
        id: canvas
        x: combobox.width - width - combobox.rightPadding
        y: combobox.topPadding + (combobox.availableHeight - height) / 2
        width: 12
        height: 8
        contextType: "2d"

        Connections {
            target: combobox
            function onPressedChanged() { canvas.requestPaint(); }
        }

        onPaint: {
            context.reset();
            context.moveTo(0, 0);
            context.lineTo(width, 0);
            context.lineTo(width / 2, height);
            context.closePath();
            context.fillStyle = "gray";
            context.fill();
        }
    }

    popup: Popup {
        y: combobox.height - 1
        width: combobox.width
        implicitHeight: contentItem.implicitHeight
        padding: 1

        contentItem: ListView {
            clip: true
            implicitHeight: contentHeight
            model: combobox.popup.visible ? combobox.delegateModel : null
            currentIndex: combobox.highlightedIndex

            ScrollIndicator.vertical: ScrollIndicator { }
        }

        background: Rectangle {
            implicitWidth: 120
            implicitHeight: 50
            radius: 10
            color: "#4a4a4a"
        }
    }
}