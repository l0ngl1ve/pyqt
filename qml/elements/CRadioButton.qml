import QtQuick 2.7
import QtQuick.Controls 2.2

RadioButton {
    id: control

    indicator: Rectangle {
        implicitWidth: 26
        implicitHeight: 26
        x: control.leftPadding
        y: parent.height / 2 - height / 2
        radius: 13
        color: "transparent"
        border.color: "#5ae4aa"
        border.width: 2

        Rectangle {
            width: 16
            height: 16
            x: 5
            y: 5
            radius: 10
            color: "white"
            visible: control.checked
        }
    }

    contentItem: CText {
        text: control.text
        font: control.font
        leftPadding: control.indicator.width + control.spacing
    }
}
