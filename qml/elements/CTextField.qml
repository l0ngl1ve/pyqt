import QtQuick 2.7
import QtQuick.Controls 2.2


TextField {
    color: "white"
    font.pixelSize: 18

    background: Rectangle {
        implicitHeight: 50
        radius: 10
        color: "#4a4a4a"
    }
}