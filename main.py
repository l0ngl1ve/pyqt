import sys

from PyQt5.QtCore import QUrl
from PyQt5.QtQml import QQmlApplicationEngine
from PyQt5.QtWidgets import QApplication
from database import Database


class Application:
    def __init__(self, path):
        self.app = QApplication(sys.argv)
        self._db = Database()

        self.engine = QQmlApplicationEngine()
        self.engine.rootContext().setContextProperty("db", self._db)
        self.engine.load(QUrl(path))

    def run(self):
        self.engine.quit.connect(self.app.quit)
        sys.exit(self.app.exec_())


if __name__ == '__main__':
    Application('qml/base.qml').run()
