import sys

from PyQt5 import QtWidgets
from PyQt5.QtCore import QEvent, QObject
from PyQt5.QtGui import QColor

from pyqt.widgets.widgets import Ui_MainWindow


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.col = None
        self.filter = None
        self.additional_setup_ui()

    def additional_setup_ui(self):
        self.col = QColor(0, 0, 0)
        self.toggle_buttons()
        self.combo_box()

    def toggle_buttons(self):
        self.ui.pushButton.clicked[bool].connect(self.setColor)
        self.ui.pushButton_2.clicked[bool].connect(self.setColor)
        self.ui.pushButton_3.clicked[bool].connect(self.setColor)

    def setColor(self, pressed):
        source = self.sender()
        val = 255 if pressed else 0

        if source.text() == 'Red':
            self.col.setRed(val)
            print('red')
        elif source.text() == "Green":
            self.col.setGreen(val)
            print('green')
        else:
            self.col.setBlue(val)
            print('blue')

        self.ui.frame.setStyleSheet("QFrame { background-color: %s }" % self.col.name())

    def combo_box(self):
        self.ui.comboBox.activated[str].connect(self.onActivated)
        self.filter = ClickComboBox()
        self.ui.comboBox.installEventFilter(self.filter)

    def onActivated(self, text):
        print(f'New combobox value: {text}')
        self.ui.label_14.setText(text)
        self.ui.label_14.adjustSize()


class ClickComboBox(QObject):
    def eventFilter(self, obj, event):
        if event.type() == QEvent.MouseButtonPress:
            print('click')
            obj.addItems(['milk', 'sugar', 'honey', 'water'])
        return super(ClickComboBox, self).eventFilter(obj, event)


if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    app2 = MainWindow()
    app2.show()
    sys.exit(app.exec())
