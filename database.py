from PyQt5.QtCore import QObject, pyqtSignal, pyqtProperty, pyqtSlot
import requests


class Database(QObject):
    def __init__(self):
        super(Database, self).__init__()
        self.__token = None

        self.auth()
        self._locations = []

    signalGetLocations = pyqtSignal()

    def auth(self):
        auth = requests.post('http://127.0.0.1:8000/api_v1/auth_api/', data=({'username': 'admin', 'password': '1234'}))
        if auth.ok:
            self.__token = auth.json()['token']

    @pyqtSlot(name='getLocations')
    def get_locations(self):
        r = requests.get('http://127.0.0.1:8000/api_v1/general/prod_location/',
                         headers={'Authorization': f'token {self.__token}'})
        self._locations = list(i['location'] for i in r.json())
        self.signalGetLocations.emit()

    comboboxLocations = pyqtProperty(list, fget=lambda self: self._locations, notify=signalGetLocations)
